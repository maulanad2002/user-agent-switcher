#!/bin/sh
set -eu #-o pipefail

cd "$(dirname "$(readlink -f "${0}")")/.."
export PATH="$(pwd)/node_modules/web-ext/bin:${PATH}"

help() {
	echo "Usage: ${0##*/} [-h] [-a]"
	echo
	echo "optional arguments:"
	echo "  -a, --auto          Try to automatically increment the version number"
	echo "                      in the manifest if it needs updating"
	echo "  -p, --push          Push the repository and the new webextension file after building"
	echo "  -S, --no-sign       Do not sign the release version tag"
	echo "  -h, --help          Show this help and exit"
	echo
	echo "exit codes:"
	echo "   1. General failure"
	echo "   2. Failed to detect current version number"
	echo "  11. Uncomitted files present in repository"
	echo "  12. Current version was already released (and -a not used)"
	echo "  21. There have been no changes since the last release"
	echo "  64. Command-line parsing error"
	echo
	echo "  Additionally, exit codes of the following commands will be forwarded as-is:"
	echo "    * git [commit|push|tag]"
	echo "    * sed"
	echo "    * web-ext [build|sign]"
}

AUTO_VERSION=false
AUTO_PUSH=false
SIGN_TAG=true
while getopts ":apSh" OPTVAR;
do
	OPTVAL="${1}"
	OPTIND=1
	shift 1
	if [ ${#OPTVAL} -gt 2 ] && [ -z "${OPTARG}" ];
	then
		set -- "-${OPTVAL##-${OPTVAR}}"
	fi

	case "${OPTVAR}" in
		a)
			AUTO_VERSION=true
		;;

		p)
			AUTO_PUSH=true
		;;

		S)
			SIGN_TAG=false
		;;

		h)
			help
			exit 0
		;;

		?)
			case "${OPTVAL}" in
				# Replace long-opts by short opts
				--auto)     set -- -a "$@" ;;
				--push)     set -- -p "$@" ;;
				--no-sign)  set -- -S "$@" ;;
				--help)     set -- -h "$@" ;;

				# Break up long-opts asignments
				--*=*)
					set -- "${OPTVAL%%=*}" "${OPTVAL#*=}" "$@"
				;;

				*)
					echo "Unsupported argument: ${OPTVAL}" >&2
					echo >&2
					help >&2
					exit 64
				;;
			esac
	esac
done

DISTFILE="web-ext-artifacts/user-agent-switcher.xpi"

###########################################
# Make sure everything has been committed #
###########################################
if [ -n "$(git status --short)" ];
then
	echo "There are uncommitted files in your repository, please commit or stash them before continuing" >&2
	exit 11
fi

#########################################################################
# Ensure that there has been at least one new commit since last release #
#########################################################################
if git describe --exact-match HEAD 2>/dev/null | grep -qE '^v[0-9]+([.-]|$)';
then
	echo "There have been no new commits since the last release" >&2
	exit 21
fi


############################
# Determine target version #
############################
version="$(grep -P --only-matching '"version":\s*"[^"]+"' "manifest.json" | cut -d'"' -f4)"
if [ -z "${version}" ];
then
	echo "Could not detect the current version number" >&2
	exit 2
fi

######################################
# Make sure version number is unique #
######################################
if git show-ref "v${version}" >/dev/null;
then
	if ${AUTO_VERSION};
	then
		if echo "${version}" | grep -qP "^\d+$";
		then
			version="${version}.0.1"
		elif echo "${version}" | grep -qP "^\d+\.\d+$";
		then
			version="${version}.1"
		elif echo "${version}" | grep -qP "^\d+\.\d+\.\d+$";
		then
			version_minor="$(echo "${version}" | grep -oP "^\d+\.\d+")"
			version_patch="$(echo "${version}" | grep -oP "\d+$")"
			version="${version_minor}.$((${version_patch}+1))"
		else
			echo "Format of current version (${version}) not recognized, cannot increment it" >&2
			exit 12
		fi

		# Write new version number
		sed -i -r 's/"version":\s*"[^"]+"/"version": "'"${version}"'"/' "manifest.json"

		# Commit this change
		git commit -m "Release version ${version}" "manifest.json"
	else
		echo "Release ${version} already exists, please increment the version number in the \"manifest.json\" file or use --auto" >&2
		exit 12
	fi
fi

################################
# Build release extension file #
################################
TEMPFILE="$(web-ext build --overwrite-dest | tee /dev/stderr | grep "^Your web extension is ready:" | cut -d':' -f2- | cut -b2-)"
code=$?
if [ $code -ne 0 ];
then
	exit $code
fi
if [ -z "${TEMPFILE}" ];
then
	echo "Could not detect \`web-ext\` target file path" >&2
	exit 1
fi

#################################
# Tag the newly created version #
#################################
if ${SIGN_TAG};
then
	git tag --sign -m "Version ${version}" "v${version}"
else
	git tag -m "Version ${version}" "v${version}"
fi

#########################
# Archive the final XPI #
#########################
DISTFILE_DIRNAME="$(dirname "${DISTFILE}")"
DISTFILE_SUFFIX="$(echo "${DISTFILE}" | grep -E --only-matching "[.][a-zA-Z0-9]+$")"
DISTFILE_BASENAME="$(basename "${DISTFILE}" "${DISTFILE_SUFFIX}")"
mv "${TEMPFILE}" "${DISTFILE_DIRNAME}/${DISTFILE_BASENAME}-${version}${DISTFILE_SUFFIX}"

if ${AUTO_PUSH};
then
	scripts/publish.sh
fi
